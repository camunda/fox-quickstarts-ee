package com.camunda.fox.quickstart.escalation.implicit.extension;

import com.camunda.fox.quickstart.escalation.implicit.Helper;
import java.util.logging.Logger;
import org.activiti.engine.ActivitiException;
import org.activiti.engine.delegate.TaskListener;
import org.activiti.engine.impl.bpmn.behavior.UserTaskActivityBehavior;
import org.activiti.engine.impl.bpmn.parser.AbstractBpmnParseListener;
import org.activiti.engine.impl.calendar.DurationHelper;
import org.activiti.engine.impl.pvm.delegate.ActivityBehavior;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.impl.pvm.process.ScopeImpl;
import org.activiti.engine.impl.util.xml.Element;

/**
 *
 * @author nico.rehwaldt
 */
public class EscalationParseListener extends AbstractBpmnParseListener {

  private Logger log = Logger.getLogger(EscalationParseListener.class.getName());
  
  @Override
  public void parseUserTask(Element element, ScopeImpl scope, ActivityImpl activity) {
    Element extensionElements = element.element(Helper.ELEMENT_NAME_EXTENSION_ELEMENTS);
    if (extensionElements != null) {
      Element escalateAfterElement = extensionElements.elementNS(Helper.FOX_NS, Helper.ESCALATE_AFTER_ELEMENT_NAME);
      
      if (escalateAfterElement != null) {
        DurationHelper escalationDuration = Helper.parseEscalateAfterDuration(escalateAfterElement);
        
        // Could set activity property and read it in our task listener
        // activity.setProperty(Helper.ESCALATE_AFTER_ELEMENT_NAME, escalateAfterString);
        
        // instead we will create a specialized task listener for each user task to escalate
        addEscalationTaskListener(activity, escalationDuration);
      }
    }
  }

  /**
   * Add escalation task listener to the activity
   * @param activity 
   */
  private void addEscalationTaskListener(ActivityImpl activity, DurationHelper escalationDuration) {
    
    ActivityBehavior behaviour = activity.getActivityBehavior();
    
    if (!(behaviour instanceof UserTaskActivityBehavior)) {
      throw new IllegalArgumentException("Supplied activiti does not represent a user task");
    }
    
    UserTaskActivityBehavior userTaskBehaviour = (UserTaskActivityBehavior) behaviour;
    
    TaskListener listener = new ExtensionEscalationTaskListener(escalationDuration);
    
    log.info("Add escalation parse listener to " + activity);

    userTaskBehaviour.getTaskDefinition()
        .addTaskListener(TaskListener.EVENTNAME_CREATE, listener);
    
  }
}
