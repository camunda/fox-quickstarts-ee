package com.camunda.fox.quickstart.escalation.implicit.extension;

import com.camunda.fox.quickstart.escalation.implicit.EscalationTaskListener;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.activiti.engine.impl.calendar.DurationHelper;

/**
 * An escalation task listener which requires the engine extension {@link EscalationParseListener} to be 
 * registered with the engine. 
 * 
 * See {@link EscalationTaskListener} for a task listener which works without 
 * (for the price of performance). 
 * 
 * @author nico.rehwaldt
 */
public class ExtensionEscalationTaskListener implements TaskListener, Serializable {

  private static Logger log = Logger.getLogger(ExtensionEscalationTaskListener.class.getName());
  
  private DurationHelper escalationDuration;
  
  public ExtensionEscalationTaskListener(DurationHelper escalationDuration) {
    this.escalationDuration = escalationDuration;
  }
  
  @Override
  public void notify(DelegateTask task) {    
    Date dueDate = dueDateFromNow();
    task.setDueDate(dueDate);
  }
  
  /**
   * Given a certain date, return the due date 
   * @param now
   * @return 
   */
  protected Date dueDateFromNow() {
    return escalationDuration.getDateAfter();
  }
}
