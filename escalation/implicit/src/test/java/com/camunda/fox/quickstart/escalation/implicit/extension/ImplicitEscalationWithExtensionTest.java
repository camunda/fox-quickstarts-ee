package com.camunda.fox.quickstart.escalation.implicit.extension;

import com.camunda.fox.quickstart.escalation.implicit.ImplicitEscalationTestBase;
import java.util.Arrays;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngineConfiguration;
import org.activiti.engine.impl.bpmn.parser.BpmnParseListener;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.test.ActivitiRule;
import org.activiti.engine.test.Deployment;
import org.junit.Test;

import org.junit.Before;
import org.junit.Rule;

/**
 *
 * @author nico.rehwaldt
 */
public class ImplicitEscalationWithExtensionTest extends ImplicitEscalationTestBase {
  
  private static ProcessEngine __engine;
  
  @Rule
  public ActivitiRule activitiRule = new ActivitiRule(createExtendedEngine());
  
  private ProcessEngine engine;
  
  @Before
  public void before() {
    engine = activitiRule.getProcessEngine();
  }
  
  @Test
  @Deployment(resources="escalation-implicit-with-platform-extension.bpmn")
  public void shouldParseExtension() {
    
  }
  
  @Test
  @Deployment(resources="escalation-implicit-with-platform-extension.bpmn")
  public void shouldAssignDueDateToTaskWithExtension() {
    shouldHandleEscalationInProcessCorrectly(engine);
  }
  
  //
  // Helper methods 
  //
  
  private static ProcessEngine createExtendedEngine() {
    if (__engine == null) {
      ProcessEngineConfigurationImpl configuration = (ProcessEngineConfigurationImpl) 
        ProcessEngineConfiguration.createProcessEngineConfigurationFromResource("activiti.cfg.xml");

      configuration.setCustomPostBPMNParseListeners(Arrays.asList(new BpmnParseListener[] { new EscalationParseListener() }));

      __engine = configuration.buildProcessEngine();
    }
    
    return __engine;
  }
}
