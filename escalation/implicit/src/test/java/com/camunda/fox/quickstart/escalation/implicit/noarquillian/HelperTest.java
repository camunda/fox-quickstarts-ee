package com.camunda.fox.quickstart.escalation.implicit.noarquillian;

import java.io.InputStream;
import org.junit.Test;
import org.w3c.dom.Element;

import com.camunda.fox.quickstart.escalation.implicit.Helper;

import static org.junit.Assert.*;

/**
 *
 * @author nico.rehwaldt
 */
public class HelperTest {

  @Test
  public void shouldDeployBPMN20Diagram() {
    InputStream inputStream = this.getClass().getResourceAsStream("/escalation-implicit-with-platform-extension.bpmn");
    Element taskExtensions = Helper.getUserTaskExtensions(inputStream, "categorize-invoice", Helper.ESCALATE_AFTER_ELEMENT_NAME);
    
    assertNotNull(taskExtensions);
    assertEquals("PT1M", taskExtensions.getAttribute(Helper.ESCALATE_AFTER_ATTR_DURATION));
  }
}
