package com.camunda.fox.quickstart.tasklist.performance;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.runtime.ProcessInstance;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.DependencyResolvers;
import org.jboss.shrinkwrap.resolver.api.maven.MavenDependencyResolver;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import static org.junit.matchers.JUnitMatchers.*;
import org.junit.runner.RunWith;

@RunWith(Arquillian.class)
public class ArquillianTestCase {

  @Deployment  
  public static WebArchive createDeployment() {
    MavenDependencyResolver resolver = DependencyResolvers.use(MavenDependencyResolver.class)
      .goOffline()
      .loadMetadataFromPom("pom.xml");
     
    return ShrinkWrap.create(WebArchive.class, "quickstart-tasklist-performance.war")            
    		.addAsLibraries(resolver.artifact("com.camunda.fox.platform:fox-platform-client").resolveAs(JavaArchive.class))
            .addAsWebResource("META-INF/test-processes.xml", "WEB-INF/classes/META-INF/processes.xml")
            .addAsWebResource("META-INF/beans.xml", "WEB-INF/classes/META-INF/beans.xml")
            .addAsWebResource("META-INF/test-persistence.xml", "WEB-INF/classes/META-INF/persistence.xml")
            
            .addPackages(false, "com.camunda.fox.quickstart.tasklist.performance")
            
            .addAsResource("oneTaskProcess.bpmn")
            .addAsResource("customTaskMappings.xml")
            .addAsResource("mappings.xml");    
  }

  @After
  public void after() {
    customerService.removeAll();
  }
  
//  @Inject
//  private ProcessEngine processEngine;

  @Inject
  private RuntimeService runtimeService;

//  @Inject
//  private TaskService taskService;
//  
//  @Inject
//  private HistoryService historyService;
  
  @Inject
  private TasklistService tasklistService;

  @Inject
  private CustomerService customerService;

  @Test
  public void test() throws Exception {
    // create unique city
    String regionUnderTest = "Berlin." + System.currentTimeMillis();

    Customer customer = new Customer();
    customer.setName("camunda");
    customer.setRegion(regionUnderTest);
    customerService.persist(customer);
    long customerId = customer.getId();
    
    HashMap<String, Object> variables = new HashMap<String, Object>();
    variables.put("customerId", customerId);
    variables.put("someContent", "0815");
    
    ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("oneTaskProcess", variables);
    
    System.out.println("Started process instance id " + processInstance.getId());
    
    assertThat(runtimeService.getActiveActivityIds(processInstance.getId()), hasItem("theTask"));
    
    List<TaskDTO> tasksForRegion = tasklistService.getTasksForRegion("kermit", regionUnderTest);
    assertEquals(1, tasksForRegion.size());
    TaskDTO task = tasksForRegion.get(0);

    assertNotNull(task.getCustomer());
    assertEquals(regionUnderTest, task.getCustomer().getRegion());
    assertEquals("camunda", task.getCustomer().getName());
    assertEquals(customerId, task.getCustomer().getId());
    
    assertEquals(2, task.getVariables().size());
//    assertThat(task.getVariables(), hasItem("theTask"));
  }
}
