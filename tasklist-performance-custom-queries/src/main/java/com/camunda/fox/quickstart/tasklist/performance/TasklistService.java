package com.camunda.fox.quickstart.tasklist.performance;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Named;

import org.activiti.engine.impl.ProcessEngineImpl;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;

import com.camunda.fox.client.impl.ProcessArchiveSupport;
import com.camunda.fox.platform.api.ProcessEngineService;

@Stateless
@Named
public class TasklistService {

  @EJB(lookup = ProcessArchiveSupport.PROCESS_ENGINE_SERVICE_NAME)
  private ProcessEngineService processEngineService;

  // @EJB
  // private ProcessArchiveSupport processArchiveSupport;

  public List<TaskDTO> getTasksForRegion(final String assignee, final String region) {
    ProcessEngineImpl processEngine = (ProcessEngineImpl) processEngineService.getDefaultProcessEngine();
    ProcessEngineConfigurationImpl processEngineConfiguration = processEngine.getProcessEngineConfiguration();

    MyBatisQueryCommandExecutor commandExecutor = new MyBatisQueryCommandExecutor(processEngineConfiguration, "mappings.xml");
    return commandExecutor.executeQueryCommand(new Command<List<TaskDTO>>() {

      @SuppressWarnings("unchecked")
      public List<TaskDTO> execute(CommandContext commandContext) {

        // TODO: Add more parameters
        //        ListQueryParameterObject queryParameterObject = new ListQueryParameterObject();
        //        queryParameterObject.setParameter(region);
        //        queryParameterObject.setAssignee(assignee);

        // select the first 100 elements for this query
        return (List<TaskDTO>) commandContext.getDbSqlSession().selectList("selectTasksForRegion", region, 0, 100);
      }
    });
  }

}
