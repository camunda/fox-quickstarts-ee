# Introduction
This quickstart shows a way to implement a "4 eyes principle" where something has to be done
by two people one after the other, where the second person has to be different from the first.

This quickstart gives a more sophisticated implementation, which you might want to use if you have
this problem in many situations throughout your application, which motivates a more generic and
configurable approach than we have given in the four-eyes-simple quickstart. 


# Environment Restrictions



# Remarks to run this quickstart
There is no web interface to access the application, please refer to the Arquillian Test case to get started, which by
default connects to a local running fox platform on JBoss AS 7.

If you want to check out the parse listener part of the solution refer to https://app.camunda.com/confluence/display/foxUserGuide/Extending+the+configuration


# Known Issues fixed with alpha7: 
* Remove own copy of Bpmn20NamespaceContext (got extendible)
* Add possibility to query class name from ClassDelegate to check for existing listeners (see FourEyesExtensionsParseListener)
* http://jira.codehaus.org/browse/ACT-1216: When recycling execution it is not activated correctly (LET ONE TEST FAIL AT THE MOMENT!) 

# Known Issues:
* http://jira.codehaus.org/browse/ACT-1065: TaskListeners cannot be CDI beans at the moment (so we used normal Java Classes)

# Improvements backlog
* We cannot add BpmnParseListeners on-the-fly within Arquillian Tests, so you would have to add them manually. Discuss.
* We should provide API support for extensions or a proper helper for the XML handling (https://bitbucket.org/camunda/fox-quickstarts/src/f1eab40eb2a9/four-eyes-advanced/src/main/java/com/camunda/fox/quickstart/usertask/foureyes/advanced/Helper.java)
