package com.camunda.fox.quickstart.usertask.foureyes.advanced;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import org.activiti.engine.impl.identity.Authentication;

/**
 * Replace this by your implementation or inject the 
 * Principal in a Java EE 6 environment
 */
@Named
public class AuthenticationService {
  
  public String getLoggedInUser() {
    if (Authentication.getAuthenticatedUserId()!=null) {
      // we are running the fox tasklist environment, so this is set to the logged in user / use it!
      return Authentication.getAuthenticatedUserId();
    }
    return "test";
  }

  public List<String> getGroupsOfLoggedInUser() {
    ArrayList<String> list = new ArrayList<String>();
    list.add("test");
    list.add("management");
    return list;
  }

}
