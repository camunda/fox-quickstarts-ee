package com.camunda.fox.quickstart.usertask.foureyes.advanced;

import org.activiti.engine.impl.bpmn.behavior.UserTaskActivityBehavior;
import org.activiti.engine.impl.bpmn.helper.ClassDelegate;
import org.activiti.engine.impl.bpmn.parser.AbstractBpmnParseListener;
import org.activiti.engine.impl.bpmn.parser.BpmnParseListener;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.impl.pvm.process.ScopeImpl;
import org.activiti.engine.impl.util.xml.Element;

/**
 * {@link BpmnParseListener} which reads additional configuration from the BPMN
 * 2.0 XML and provides them via Properties during runtime, see
 * {@link TaskListService} for a usage example.
 * 
 * This is the most convenient way to read the extensions (and to validate them
 * during deployment time) but it has to be configured into the platform, see <a
 * href=
 * "https://app.camunda.com/confluence/display/foxUserGuide/Extending+the+configuration"
 * >fox user guide</a> for more information on this.
 * 
 * This is why we do not use it in the Arquillian test case and work our way
 * around this, to have less dependencies on the environment for the quickstart.
 * 
 * @author ruecker
 */
public class FourEyesExtensionsParseListener extends AbstractBpmnParseListener {

  public void parseUserTask(Element element, ScopeImpl scope, ActivityImpl activity) {
    Element extensionElements = element.element(Helper.ELEMENT_NAME_EXTENSION_ELEMENTS);
    if (extensionElements != null) {
      Element fourEyesGroupName = extensionElements.elementNS(Helper.FOX_NS, Helper.FOUR_EYES_GROUP_NAME);
      if (fourEyesGroupName != null) {
        String fourEyesGroupNameString = fourEyesGroupName.getText();
        activity.setProperty(Helper.FOUR_EYES_GROUP_NAME, fourEyesGroupNameString);

        addFourEyesTaskListener(activity);
      }
    }

  }

  /**
   * Add TaskListener on "complete" event generically every time, so we don't
   * have to add it in the XML. Since we have not enabled this ParseListener in
   * this quickstart, we have to add it to the XML manually anyway.
   */
  private void addFourEyesTaskListener(ActivityImpl activity) {
    UserTaskActivityBehavior userTaskActivityBehavior = (UserTaskActivityBehavior) activity.getActivityBehavior();

    // check that the listener wasn't added in the XML manually
//  List<TaskListener> existingListeners = userTaskActivityBehavior.getTaskDefinition().getTaskListeners().get("complete");
//  for (TaskListener taskListener : existingListeners) {
    // TODO: Change? We cannot check the class name of the listener, so we cannot check if it is already there!
    // if (taskListener instanceof ClassDelegate && ((ClassDelegate)taskListener).)
//  }
    
    ClassDelegate taskListener = new ClassDelegate(TaskCompletionListener.class, null);
    userTaskActivityBehavior.getTaskDefinition().addTaskListener("complete", taskListener);

  }

}
