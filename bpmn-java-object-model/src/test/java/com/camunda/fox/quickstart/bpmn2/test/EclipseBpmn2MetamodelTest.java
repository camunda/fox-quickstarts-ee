package com.camunda.fox.quickstart.bpmn2.test;

import com.camunda.fox.bpmn2.util.Bpmn2Extensions;
import com.camunda.fox.bpmn2.util.Filter;
import com.camunda.fox.bpmn2.util.First;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.*;
import org.eclipse.bpmn2.*;
import org.eclipse.bpmn2.util.Bpmn2ResourceFactoryImpl;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.xml.type.AnyType;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 *
 * @author nico.rehwaldt
 */
public class EclipseBpmn2MetamodelTest {

  @Test
  public void shouldReadMetaModel() throws Exception {

    Resource resource = createResource("test.bpmn");
    resource.load(getResourceAsStream("simple-service-task-process.bpmn"), Collections.EMPTY_MAP);
    
//    Bpmn2Factory factory = Bpmn2Factory.eINSTANCE;
    
    Definitions d = (Definitions) resource.getContents().get(0).eContents().get(0);
    org.eclipse.bpmn2.Process process = (org.eclipse.bpmn2.Process) d.getRootElements().get(0);
    
    assertEquals("Default Process", process.getName());
    
    List<ServiceTask> serviceTasks = Filter.byType(ServiceTask.class, process.getFlowElements());
    assertEquals(1, serviceTasks.size());
    
    ServiceTask serviceTask = First.byType(ServiceTask.class, process.getFlowElements());
    
    List<Map<String, Object>> extensions = Bpmn2Extensions.get(serviceTask, "http://www.camunda.com/fox", "elementFoo");
    assertEquals(1, extensions.size());
    
    assertEquals("bar", extensions.get(0).get("value"));

    ExtensionAttributeValue extensionValue = First.byType(ExtensionAttributeValue.class, serviceTask.getExtensionValues());
    FeatureMap.Entry entry = extensionValue.getValue().get(0); 
    
    // Check if we contain a bar attribute
    assertEquals("bar", ((AnyType) entry.getValue()).getAnyAttribute().get(0).getValue());
    
    /***************** Add custom extension *************************/ 
    
    Map<String, Object> extensionAttributes = new HashMap<String, Object>();
    extensionAttributes.put("myattribute", "asd");
    Bpmn2Extensions.add(serviceTask, "http://www.camunda.com/fox", "fooBar", extensionAttributes);
    
    // Can also add extension elements to arbitray model elements
    Bpmn2Extensions.add(process, "http://www.camunda.com/fox", "fooBar", extensionAttributes);
    
    /***************** Safe stuff ***********************************/ 
    
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    resource.save(out, null);
    
    assertTrue(out.toString().contains("<fox:fooBar xsi:type=\"xsd:anyType\" myattribute=\"asd\"/>"));
  }

  @Test
  public void shouldMakeUsAddAUserTask() throws Exception {
    Resource resource = createResource("test.bpmn");
    resource.load(getResourceAsStream("simple-service-task-process.bpmn"), Collections.EMPTY_MAP);
    
    Definitions d = (Definitions) resource.getContents().get(0).eContents().get(0);
    org.eclipse.bpmn2.Process process = (org.eclipse.bpmn2.Process) d.getRootElements().get(0);
    
    StartEvent startEvent = First.byType(StartEvent.class, process.getFlowElements());
    
    SequenceFlow flowFromStartEvent = startEvent.getOutgoing().get(0);
    
    // we traverse the first sequence flow from our start event
    ServiceTask serviceTask = (ServiceTask) flowFromStartEvent.getTargetRef();
    
    // Our factory for creating BPMN2 elements
    Bpmn2Factory factory = Bpmn2Factory.eINSTANCE;
    
    // Create a user task + another sequence flow
    UserTask userTask = factory.createUserTask();
    userTask.setId("myUserTask");
    
    SequenceFlow flowToServiceTask = factory.createSequenceFlow();
    
    // Wire them together with the existing model elements
    flowFromStartEvent.setTargetRef(userTask);
    flowToServiceTask.setTargetRef(serviceTask);
    flowToServiceTask.setSourceRef(userTask);
    
    // add them to the process
    process.getFlowElements().add(userTask);
    process.getFlowElements().add(flowToServiceTask);
    
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    resource.save(out, null);
    
    assertTrue(out.toString().contains("<bpmn2:userTask id=\"myUserTask\">"));
  }
  
  protected Resource createResource(String name) throws URISyntaxException {
    URI uri = URI.createURI(name);
    return new Bpmn2ResourceFactoryImpl().createResource(uri);
  }
  
  protected InputStream getResourceAsStream(String name) {
    return getClass().getClassLoader().getResourceAsStream(name);
  }
  
  public void print(Iterator<?> iterator) {
    System.out.println("---------- ");
    while (iterator.hasNext()) {
      System.out.println(iterator.next());
    }
    System.out.println("---------- ");
  }
}
