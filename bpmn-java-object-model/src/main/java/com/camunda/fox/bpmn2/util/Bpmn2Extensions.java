package com.camunda.fox.bpmn2.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.eclipse.bpmn2.BaseElement;
import org.eclipse.bpmn2.Bpmn2Factory;
import org.eclipse.bpmn2.Bpmn2Package;
import org.eclipse.bpmn2.ExtensionAttributeValue;
import org.eclipse.bpmn2.util.XmlExtendedMetadata;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.impl.EAttributeImpl;
import org.eclipse.emf.ecore.impl.EStructuralFeatureImpl;
import org.eclipse.emf.ecore.util.ExtendedMetaData;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.xml.type.AnyType;
import org.eclipse.emf.ecore.xml.type.XMLTypeFactory;
import org.eclipse.emf.ecore.xml.type.impl.AnyTypeImpl;

/**
 *
 * @author nico.rehwaldt
 */
public class Bpmn2Extensions {
  
  public static Map<String, Object> filterAttributes(BaseElement element, String namespace, String tag) {
    return filterFeatureMap(element.getAnyAttribute(), namespace, tag);
  }
  
  public static Map<String, Object> filterFeatureMap(FeatureMap featureMap, String namespace, String tag) {
    Map<String, Object> attributes = new HashMap<String, Object>();
    
    for (FeatureMap.Entry attribute: featureMap) {
      if (!isInFilter(attribute.getEStructuralFeature(), namespace, tag)) {
        continue;
      }
      
      String name = attribute.getEStructuralFeature().getName();
      Object value = attribute.getValue();

      attributes.put(name, value);
    }
    
    return attributes;
  }
  
  private static boolean isInFilter(EStructuralFeature eStructuralFeature, String namespace, String tag) {
    
    ExtendedMetaData metadata = ExtendedMetaData.INSTANCE;
    
    if (namespace != null) {
      String extensionNs = metadata.getNamespace(eStructuralFeature);

      if (!namespace.equals(extensionNs)) {
        return false;
      }
    }

    if (tag != null) {
      String extensionTag = eStructuralFeature.getName();

      if (!tag.equals(extensionTag)) {
        return false;
      }
    }
    return true;
  }
  
  /**
   * Returns extensions for a given object
   * @param object
   * @param namespace
   * @return 
   */
  public static List<Map<String, Object>> get(BaseElement object, String namespace, String tag) {
    List<Map<String, Object>> extensions = new ArrayList();
    
    ExtensionAttributeValue extensionAttributes = getOrCreate(ExtensionAttributeValue.class, object.getExtensionValues());
    if (extensionAttributes != null) {
      for (FeatureMap.Entry extension: extensionAttributes.getValue()) {
        
        if (!isInFilter(extension.getEStructuralFeature(), namespace, tag)) {
          continue;
        }
        
        Map<String, Object> tagAttributes = null;
        Object extensionValue = extension.getValue();

        if (extensionValue instanceof BaseElement) {
          BaseElement baseElement = (BaseElement) extensionValue;
          tagAttributes = filterFeatureMap(baseElement.getAnyAttribute(), null, null);
        } else 
        if (extensionValue instanceof AnyType) {
          AnyType anyType = (AnyType) extensionValue;
          tagAttributes = filterFeatureMap(anyType.getAnyAttribute(), null, null);
        } else {
          throw new IllegalArgumentException("Fail to parse extension value " + extensionValue);
        }
          
        extensions.add(tagAttributes);
      }
    }
    
    return extensions;
  }
  
  public static void add(BaseElement object, String namespace, String tag, Map<String, Object> attributes) {
    
    ExtendedMetaData metadata = XmlExtendedMetadata.INSTANCE;
    
    ExtensionAttributeValue extensionAttributes = getOrCreate(ExtensionAttributeValue.class, object.getExtensionValues());
    FeatureMap extensions = extensionAttributes.getValue();
    
    // create extension element type
    EStructuralFeature extensionElementType = metadata.demandFeature(namespace, tag, true, true);
    
    extensionElementType.setChangeable(true);
    
    AnyType extensionElement = (AnyTypeImpl) XMLTypeFactory.eINSTANCE.createAnyType();
    
    // add extension element to extensions
    extensions.add(extensionElementType, extensionElement);  
    
    // create extension attributes
    for (Map.Entry<String, Object> attribute: attributes.entrySet()) {
      
      String ns = null;
      
      String name = attribute.getKey();
      Object value = attribute.getValue();
      
      // name can be (namespace):name to assign a namespace to it
      Pattern pattern = Pattern.compile("^\\((.*)\\):(.*)$");
      Matcher matcher = pattern.matcher(name);

      // name can be (namespace):name to assign a namespace to it
      if (matcher.matches()) {
        ns = matcher.group(1);
        name = matcher.group(2);
      }

      // single extension attribute
      EAttributeImpl extensionAttributeType = (EAttributeImpl) metadata.demandFeature(
            ns, name, false, false);
      
      // add it to extension element
      extensionElement.getAnyAttribute().add(new EStructuralFeatureImpl.SimpleFeatureMapEntry(extensionAttributeType, value));
    }
  }
  
  public static <T extends EObject> T getFirstUnlessEmpty(Class<T> cls, List<T> elements) {
    if (elements.isEmpty()) {
      return null;
    }
    
    return elements.get(0);
  }
  
  public static <T extends EObject> T getOrCreate(Class<T> cls, List<T> elements) {
    if (elements.isEmpty()) {
      EClass ecls = classToEClassMap.get(cls);
      if (ecls == null) {
        ecls = scanForEClass(cls, Bpmn2Package.eINSTANCE.getEClassifiers());
        if (ecls == null) {
          return null;
        }
      }
      
      T element = (T) Bpmn2Factory.eINSTANCE.create(ecls);
      elements.add(element);
    }
    
    return elements.get(0);
  }
  
  private static EClass scanForEClass(Class<?> cls, List<EClassifier> classifiers) {
    
    String clsName = cls.getSimpleName();
    
    for (EClassifier classifier: classifiers) {
      if (cls.getName().equals(classifier.getInstanceClassName()) ||
          clsName.equals(classifier.getName()) && 
          (classifier instanceof EClass)) {
        
        return (EClass) classifier;
      }
    }
    
    return null;
  }
  
  private static final Map<Class<?>, EClass> classToEClassMap = new HashMap<Class<?>, EClass>();
}
