package com.camunda.fox.quickstart.jmx.monitoring;

import java.util.Date;
import java.util.List;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.history.HistoricProcessInstance;

/**
 *
 * @author nico.rehwaldt
 */
public class EngineMonitor implements EngineMonitorMXBean {
  
  private final long MILLISECONDS_PER_DAY = 86400000l;
  
  private final ProcessEngine processEngine;
  
  public EngineMonitor(ProcessEngine processEngine) {
    this.processEngine = processEngine;
  }

  @Override
  public synchronized long getRunningProcessInstanceCount() {
    return processEngine.getRuntimeService().createProcessInstanceQuery().count();
  }

  @Override
  public long getProcessInstancesProcessingTimeLastDay() {
    Date now = new Date();
    Date oneDayEarlier = new Date(now.getTime() - MILLISECONDS_PER_DAY);
    
    List<HistoricProcessInstance> historicInstanceLastDay = 
      processEngine
        .getHistoryService()
          .createHistoricProcessInstanceQuery()
            .startDateBy(oneDayEarlier)
            .finished()
            .list();
    
    long sum = 0;
    long count = 0;
    
    for (HistoricProcessInstance instance: historicInstanceLastDay) {
      sum += instance.getDurationInMillis();
      count++;
    }
    
    return count == 0 ? -1l : sum / count;
  }

  @Override
  public long getFailedJobsCount() {
    return processEngine.getManagementService().createJobQuery().withException().count();
  }
  
  public String getEngineName() {
    return processEngine.getName();
  }
}
