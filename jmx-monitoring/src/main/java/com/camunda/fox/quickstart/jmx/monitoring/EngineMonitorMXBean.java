package com.camunda.fox.quickstart.jmx.monitoring;

/**
 *
 * @author nico.rehwaldt
 */
public interface EngineMonitorMXBean {
  
  long getRunningProcessInstanceCount();
  
  long getProcessInstancesProcessingTimeLastDay();
  
  long getFailedJobsCount();
}
