package com.camunda.fox.quickstart.jmx.monitoring;

import com.camunda.fox.platform.api.ProcessEngineService;
import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.management.*;

/**
 *
 * @author nico.rehwaldt
 */
@Startup
@Singleton
public class MonitorRegistration {
  
  private static final Logger log = Logger.getLogger(MonitorRegistration.class.getName());
  
  @javax.ejb.EJB(lookup = "java:global/camunda-fox-platform/process-engine/PlatformService!com.camunda.fox.platform.api.ProcessEngineService")
  protected ProcessEngineService processEngineService;
  
  private List<ObjectName> registeredMBeanNames = new ArrayList<ObjectName>();
  
  private EngineMonitor monitor;
  
  @PostConstruct
  protected void setup() {
    monitor = new EngineMonitor(processEngineService.getDefaultProcessEngine());
    String name = "com.camunda.fox.platform.jmx" + ":type=" + "EngineMonitorMXBean,engineName=" + monitor.getEngineName();
    
    log.info("Registering MBeans");
    registerMBean(name, monitor);
  }
  
  @PreDestroy
  protected void shutdown() {
    log.info("Unregistering MBeans");
    unregisterRegisteredMBeans();
  }
  
  private void registerMBean(String name, Object bean) {
    
    MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
    try {
      
      ObjectName objectName = new ObjectName(name);		
      mbs.registerMBean(bean, objectName);
      
      registeredMBeanNames.add(objectName);
    } catch (Exception e) {
      log.warning("Failed to register MBean " + bean + " with name " + name + ": " + e);
    }
  }
  
  private void unregisterRegisteredMBeans() {
    
    MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
    
    for (ObjectName name: registeredMBeanNames) {
      try {
        mbs.unregisterMBean(name);
      } catch (Exception e) {
        log.warning("Failed to unregister MBean with name " + name + ": " + e);
      }
    }
    
    registeredMBeanNames.clear();
  }
  
  public EngineMonitor getMonitor() {
    return monitor;
  }
}
