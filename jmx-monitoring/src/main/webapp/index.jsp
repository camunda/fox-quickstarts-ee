<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>JMX Qzuickstart</title>
  </head>
  <body>
    <h1>JMX Quickstart</h1>
    <p>See <a href="https://app.camunda.com/confluence/display/foxUserGuide/Monitoring+the+fox+platform+via+JMX">fox user guide</a> for more information.</p>
    <p>
      Open a JMX client (either 
      <a href="http://docs.oracle.com/javase/6/docs/technotes/guides/management/jconsole.html">JConsole</a> or 
      <a href="http://docs.oracle.com/javase/6/docs/technotes/tools/share/jvisualvm.html">JVisualVM</a>)
      and inspect your JBoss server instance.
    </p>
  </body>
</html>
