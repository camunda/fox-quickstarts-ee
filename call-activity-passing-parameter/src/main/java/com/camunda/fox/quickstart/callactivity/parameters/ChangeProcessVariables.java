package com.camunda.fox.quickstart.callactivity.parameters;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;


public class ChangeProcessVariables implements JavaDelegate {

  @Override
  public void execute(DelegateExecution execution) throws Exception {
    execution.setVariable("x", "kristin");
    execution.setVariable("y", "robert");    
  }

}
