package com.camunda.fox.quickstart.callactivity.parameters;

import java.util.Map;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.activiti.engine.impl.persistence.entity.ExecutionEntity;

/**
 * Attach this Execution Listener to the process start event of a process model
 * in order to copy all process variables from a super process to this process
 * (if it is called)
 * 
 * @author ruecker
 */
public class RetrieveAllParametersExecutionListener implements ExecutionListener {

  @Override
  public void notify(DelegateExecution execution) throws Exception {
    ExecutionEntity executionEntity = (ExecutionEntity) execution;
    Map<String, Object> superVariables = executionEntity.getSuperExecution().getVariables();
    
    // make sure we set the variables on the root execution
    executionEntity.getProcessInstance().setVariables(superVariables);
  }

}
