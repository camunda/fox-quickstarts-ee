package com.camunda.fox.quickstart.callactivity.parameters;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.activiti.engine.impl.persistence.entity.ExecutionEntity;

public class SaveAllParametersExecutionListener implements ExecutionListener {

  @Override
  public void notify(DelegateExecution execution) throws Exception {
    // Maybe not sufficient if you want to make sure that deleted variables are
    // deleted in the super process as well, but we haven't experienced that
    // very often to be honest
    
    // Make sure that we set it on the root execution / process instance
    ((ExecutionEntity) execution).getSuperExecution().getProcessInstance().setVariables(execution.getVariables());
  }

}
