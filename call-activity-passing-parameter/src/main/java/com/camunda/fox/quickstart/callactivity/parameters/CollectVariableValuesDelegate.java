package com.camunda.fox.quickstart.callactivity.parameters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;

/**
 * {@link JavaDelegate} to collect variable values at a certain time in the process.
 * This is provided as static information. Using CDI beans would be nicer, but would limit
 * the showcase to the fox environment, here we can work with a plain fox-engine :-) 
 * 
 * @author ruecker
 */
public class CollectVariableValuesDelegate implements JavaDelegate {
  
  public static final String BUSINESS_KEY_NAME = "BUSINESS-KEY";
  
  public static List<Map<String, String>> values = new ArrayList<Map<String, String>>();

  @Override
  public void execute(DelegateExecution execution) throws Exception {
    HashMap<String, String> currentVariables = new HashMap<String, String>();
    
    Set<String> variableNames = execution.getVariableNames();
    for (String variableName : variableNames) {
      if (execution.getVariable(variableName)!=null) {
        currentVariables.put(variableName, execution.getVariable(variableName).toString());
      }
    }

    currentVariables.put(BUSINESS_KEY_NAME, execution.getProcessBusinessKey());
    
    values.add(currentVariables);
  }

}
