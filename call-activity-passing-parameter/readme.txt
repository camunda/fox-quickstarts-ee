# Introduction
This quickstart shows how to pass a business key or ALL parameters to a called process
without having to specify a mapping for all process variables like the normal way
(see http://www.activiti.org/userguide/index.html#bpmnCallActivity). Hence instead of 
writing 

<callActivity id="CallActivity" calledElement="called-process" >
  <extensionElements>
          <activiti:in source="x" target="x" />
          <activiti:out source="y" target="y" />
  </extensionElements>
</callActivity>

We add http://www.activiti.org/userguide/index.html#executionListeners to the start and end event of the 
called process which copy the necessary values. THis is now done automatically without any specific configuration:

<process id="called-process" name="called-process" isExecutable="true">
  <extensionElements>
      <activiti:executionListener event="start" class="com.camunda.fox.quickstart.callactivity.parameters.RetrieveAllParametersExecutionListener" />
      <activiti:executionListener event="start" class="com.camunda.fox.quickstart.callactivity.parameters.RetrieveBusinessKeyExecutionListener" />
      <activiti:executionListener event="end"   class="com.camunda.fox.quickstart.callactivity.parameters.SaveAllParametersExecutionListener" />
  </extensionElements>


# Environment Restrictions
This quickstart only uses a test case with the fox-engine, no fox-platform is needed at all.


# Remarks to run this quickstart
There is no web interface to access the application, please refer to the test case to get started, which by
default starts up an in memory fox-engine


# Known Issues:
