# Introduction
This quickstart shows one way to implement a "4 eyes principle" where something has to be done
by two people one after the other, where the second person has to be different from the first.

This quickstart gives a very easy implementation usable if you only have this situation once and can use
a process variable to store the last person. 


# Environment Restrictions
None


# Remarks to run this quickstart
There is no web interface to access the application, please refer to the Arquillian Test case to get started, which by
default connects to a local running fox platform on JBoss AS 7.


# Known Issues:
* http://jira.codehaus.org/browse/ACT-1213: With introduction of the not query we can even remove the tasks from the task list in the first place 
* http://jira.codehaus.org/browse/ACT-1065: TaskListeners cannot be CDI beans at the moment (so we used normal Java Classes)
