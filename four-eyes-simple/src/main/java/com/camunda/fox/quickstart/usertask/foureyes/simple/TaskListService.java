package com.camunda.fox.quickstart.usertask.foureyes.simple;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;

@Named
@Stateless
public class TaskListService {
  
  /**
   * LIMIT tasks to the given process definition to ease testing
   */
  public static final String PROCESS_DEFINITION_KEY= "four-eyes-simple";  

  @Inject
  private AuthenticationService authenticationService;

  @Inject
  private TaskService taskService;

  @Inject
  private RuntimeService runtimeService;

  public List<Task> getTaskForCurrentUser(boolean filterFourEyeTasks) {
    ArrayList<Task> tasks = new ArrayList<Task>();

    tasks.addAll(taskService.createTaskQuery().processDefinitionKey(PROCESS_DEFINITION_KEY).taskAssignee(authenticationService.getLoggedInUser()).list());
    
    if (filterFourEyeTasks) {
      tasks.addAll(taskService.createTaskQuery()
              .processDefinitionKey(PROCESS_DEFINITION_KEY)
              .taskCandidateGroupIn(authenticationService.getGroupsOfLoggedInUser())
              .processVariableValueNotEquals(Constants.PROCESS_VARIABLE_NAME, authenticationService.getLoggedInUser()).list());
    }
    else {
      tasks.addAll(taskService.createTaskQuery().processDefinitionKey(PROCESS_DEFINITION_KEY)
              .taskCandidateGroupIn(authenticationService.getGroupsOfLoggedInUser()).list());
    }

    return tasks;
  }

  public void claim(Task task) {
    check4EyesPrinciple(task);
    taskService.claim(task.getId(), authenticationService.getLoggedInUser());
  }

  public void complete(Task task) {
    check4EyesPrinciple(task);
    taskService.complete(task.getId());
  }

  private void check4EyesPrinciple(Task task) {
    String lastUser = (String) runtimeService.getVariable(task.getProcessInstanceId(), Constants.PROCESS_VARIABLE_NAME);
    if (authenticationService.getLoggedInUser().equals(lastUser)) {
      throw new RuntimeException("You cannot claim or complete task '" + task.getId() + "' because this would violate the 4 eyes principle.");
    }
  }

}
