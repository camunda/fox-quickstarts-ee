package com.camunda.fox.quickstart.usertask.foureyes.simple;

import javax.inject.Named;

import org.activiti.cdi.impl.util.ProgrammaticBeanLookup;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.activiti.engine.impl.bpmn.parser.BpmnParseListener;


/**
 * Listener to save user who completed the task as process variable. Could be added via an 
 * {@link BpmnParseListener}, then you even don't have to add it manually to the XML.
 * 
 * @author ruecker
 */
@Named
public class TaskCompletionListener implements TaskListener {
  
  @Override
  public void notify(DelegateTask task) {    
    // We cannot use a CDI Bean as TaskListener until http://jira.codehaus.org/browse/ACT-1065 is resolved
    // so we do a programatic lookup from within a normal task listener created by the engine
    AuthenticationService authenticationService = ProgrammaticBeanLookup.lookup(AuthenticationService.class);    
    
    String loggedInUser = authenticationService.getLoggedInUser();
    
    // chek that this user has not done the last task
    String lastUser = (String) task.getExecution().getVariable(Constants.PROCESS_VARIABLE_NAME);
    if (lastUser!=null && lastUser.equals(loggedInUser)) {
      throw new RuntimeException("Same user has already completed the first task, cannot complete second task, violates 4-eyes-principle");
    }
    
    if (task.getAssignee()==null || !task.getAssignee().equals(loggedInUser)) {
      // task was not assigned/claimed before completion, fix this.
      task.setAssignee(loggedInUser);
    }   
    task.getExecution().setVariable(Constants.PROCESS_VARIABLE_NAME, loggedInUser);
  }

}
