package com.camunda.fox.quickstart.usertask.foureyes.simple;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.inject.Inject;

import junit.framework.Assert;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.DependencyResolvers;
import org.jboss.shrinkwrap.resolver.api.maven.MavenDependencyResolver;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Arquillian.class)
public class FourEyesSimpleTest {

  @Deployment
  public static WebArchive createDeployment() {
    MavenDependencyResolver resolver = DependencyResolvers.use(MavenDependencyResolver.class)
      .goOffline()
      .loadMetadataFromPom("pom.xml");
    
    return ShrinkWrap
            .create(WebArchive.class, "fourEyesSimple.war")
            // prepare as process application archive for fox platform
            .addAsLibraries(resolver.artifact("com.camunda.fox.platform:fox-platform-client").resolveAs(JavaArchive.class))
            .addAsWebResource("META-INF/test-processes.xml", "WEB-INF/classes/META-INF/processes.xml")
            .addAsWebResource("META-INF/beans.xml", "WEB-INF/classes/META-INF/beans.xml")
            // add your own classes (could be done one by one as well)
            .addPackages(false, "com.camunda.fox.quickstart.usertask.foureyes.simple")            
            // add process definition
            .addAsResource("FourEyesSimple.bpmn")
    // now you can add additional stuff required for your test case
    ;
  }
  
  
//  
//  @AfterClass
//  public static void tearDown() {
//    // do it static way since we are in the AfterClass code block and doesn't get anything injected
//    ProcessEngine processEngine = ProgrammaticBeanLookup.lookup(ProcessEngine.class);
//    
//    // delete all maybe old existing process definitions to clean up
//    List<org.activiti.engine.repository.Deployment> deployments = processEngine.getRepositoryService().createDeploymentQuery().deploymentName("fourEyesSimple").list();
//    for (org.activiti.engine.repository.Deployment deployment : deployments) {
//      System.out.println("Deleting " + deployment);
//      processEngine.getRepositoryService().deleteDeployment(deployment.getId(), true);          
//    }
//  }

  @Inject
  private ProcessEngine processEngine;

  @Inject
  private TaskListService taskListService;

  /**
   * Tests that a user cannot claim or complete a task if he completed the first task
   */
  @Test
  public void testInvalidClaim() throws Exception {
    cleanUpRunningProcessInstances();
    ProcessInstance processInstance = processEngine.getRuntimeService().startProcessInstanceByKey("four-eyes-simple");

    MockAuthenticationService.loggedInUser = "bernd";
    List<Task> taskForCurrentUser = taskListService.getTaskForCurrentUser(false);
    assertEquals(1, taskForCurrentUser.size());
    
    taskListService.claim(taskForCurrentUser.get(0));
    taskListService.complete(taskForCurrentUser.get(0));

    MockAuthenticationService.loggedInUser = "bernd";
    taskForCurrentUser = taskListService.getTaskForCurrentUser(false);
    assertEquals(1, taskForCurrentUser.size());    
    try {
      taskListService.claim(taskForCurrentUser.get(0));
      Assert.fail("same user cannot claim twice");
    }
    catch (Exception ex) {
      assertTrue(ex.getMessage().contains("You cannot claim or complete task"));
    }

    MockAuthenticationService.loggedInUser = "roman";
    taskForCurrentUser = taskListService.getTaskForCurrentUser(false);
    assertEquals(1, taskForCurrentUser.size());    
    taskListService.claim(taskForCurrentUser.get(0));
    taskListService.complete(taskForCurrentUser.get(0));

    assertEquals(1, processEngine.getHistoryService().createHistoricProcessInstanceQuery().processInstanceId(processInstance.getId()).finished().count());
  }
  
  /**
   * Tests that a user cannot claim or complete a task if he completed the first task
   */
  @Test
  public void testTaskListDoesntContainTasksSomebodyCannotComplete() throws Exception {
    cleanUpRunningProcessInstances();
    
    ProcessInstance processInstance = processEngine.getRuntimeService().startProcessInstanceByKey("four-eyes-simple");

    MockAuthenticationService.loggedInUser = "bernd";
    List<Task> taskForCurrentUser = taskListService.getTaskForCurrentUser(false);
    assertEquals(1, taskForCurrentUser.size());

    // the claim is done automatically in the background
    taskListService.complete(taskForCurrentUser.get(0));

    MockAuthenticationService.loggedInUser = "bernd";
    // fails until http://jira.codehaus.org/browse/ACT-1213 is released (from alpha7 on)
    taskForCurrentUser = taskListService.getTaskForCurrentUser(true);
    assertEquals(0, taskForCurrentUser.size());    
    
    processEngine.getRuntimeService().deleteProcessInstance(processInstance.getId(), "test case cleanup");
  }
  
  /**
   * Helper to delete all running process instances which might disturb our Arquillian Test case
   * Better run test cases in a clean environment, but this is pretty handy for demo purposes
   */
  private void cleanUpRunningProcessInstances() {
    List<ProcessInstance> runningInstances = processEngine.getRuntimeService().createProcessInstanceQuery().processDefinitionKey(TaskListService.PROCESS_DEFINITION_KEY).list();
    for (ProcessInstance processInstance : runningInstances) {
      processEngine.getRuntimeService().deleteProcessInstance(processInstance.getId(), "deleted to have a clean environment for Arquillian");
    }
  }  
}
