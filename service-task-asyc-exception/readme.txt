# Introduction
This quickstart can be used to demonstrate monitoring, since it fails on a invalid input (serialNumber != 23)
and can be restarted if the value is corrected.

It is a good starting point to play around with "async=true" 

# Environment Restrictions
None known


# Remarks to run this quickstart
After deploying you can access the application: http://localhost:8080/cockpit-router-configuration/


# Known Issues:
* Currently the Arquillian TestCase just waits some for the job executor to finish, we should improve this 